# Plano de Estágio

## Descrição

Repositório de exercícios práticos desenvolvidos nos módulos do plano de estágio.

## Tópicos estudados

### Módulo 1:

1. Git
2. Markdown
3. CSS
   1. CSS Grid
   2. CSS Flexbox
4. JavaScript
   1. JavaScript - Calculadora

### Módulo 2:

1. React
2. Metodologias ágeis - Scrum
3. NPM e Yarn
4. VTEX Overview
5. Metodologias ágeis
6. APIs e Postman
   1. APIs VTEX

### Módulo 3:

1. Figma
2. Onboarding VTEX
3. VTEX IO: Conceitos

## Autor

Willian Ottoni
